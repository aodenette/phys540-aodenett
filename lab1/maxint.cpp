#include <cassert>

#include <iostream>
using std::cout;
using std::endl;

int main()
{
   int a = 1;
   int i = 9;

   cout << "2^0 = " << a << endl;
   a = a*2;

   cout << "2^1 = " << a << endl;
   a = a*2;

   cout << "2^2 = " << a << endl;
   a = a*2;

   cout << "2^3 = " << a << endl;
   a = a*2;

   cout << "2^4 = " << a << endl;
   a = a*2;

   cout << "2^5 = " << a << endl;
   a = a*2;

   cout << "2^6 = " << a << endl;
   a = a*2;

   cout << "2^7 = " << a << endl;
   a = a*2;

   cout << "2^8 = " << a << endl;
   a = a*2;

   while ( a > 0) {
	   cout << "2^" << i << " = " << a << endl;
	   a = a*2;
	   i++;
	   if ( a > a*2) {
		   a = a*2;
		   i++;
		   cout << "2^" << i << " = " << a << endl;
	   }
   }
   if ( a < 0) {
	   cout << "Error, Check last Result" << endl;
	   a = a*2;
	   i++;
	   cout << "2^" << i << " = " << a << endl;
   }     

   return 0;
}

