#include <cstdlib>
using std::atoi;

#include <algorithm>
using std::max;
using std::min;

#include <iostream>
using std::cout;
using std::endl;
//Here I decided to solve the individual addition and division
float div(float p, unsigned long int i)
{
	float val = ((p/i)+1);
	return val;
}


//in the main function, two for loops are used
//the first is to vary k, and a second within 
//each k meant to achieve the product sum
int main(int argc, char *argv[])
{
   unsigned long int n = atoi(argv[1]);
   for (unsigned long int k = 0; k <= n; ++k)
   {
	   float p = max(k,n-k);
	   unsigned long int q = min(k,n-k);
	   float fin = 1;
	   for (unsigned long int i = 1; i <= q; ++i)
	   {
	   float val3 = div(p,i);
	   fin *= val3;
	   }

      cout << "(" << n << " choose " << k << ") = " << fin << endl;
   }
   return 0;
}

