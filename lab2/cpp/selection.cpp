#include <iostream>
using std::cout;
using std::endl;
#include <vector>
using std::vector;

#include <iomanip>
using std::setw;

bool valid_l(int lf, int nf) { return lf >= 0 and lf < nf; }
bool valid_m(int mf, int lf) { return mf >= 0 and mf <= (2*lf); }

int main()
{
   const int Nmax = 20;
   vector<int> path;
   for (int n = 2; n <= Nmax; ++n) {
      for (int nf = 1; nf < n; ++nf) {
         // ksdb: there's no reason not to use a double here
	      float Ef = (13.6)/(nf*nf);
	      float Ei = (13.6)/(n*n);
	      float Edelt = Ef - Ei;
         // ksdb: You've ruined the loop here by writing
	      // if ( (nf = 1) ) {
         // which actually assigns 1 to nf each time.
         // You meant to write a boolean test
         if (nf == 1) {
            // ksdb: This line redeclares a vector<int>
            // with the name path, overwriting what you
            // did on line 16. This local path only lives
            // within the current pair of braces
		      // vector<int> path(3);
            // What you probably intended was
            path.resize(3);
            //
	      } else {
		      for (int l = 0; l < n; ++l) {
			    int  lf = l-1;
			      if ((lf =valid_l(lf, nf))) {
				      for ( int m = 0; m <= 2*l; ++m) {
					      int mf = m;
					      if ((mf = valid_m(mf,lf))) {
                        // ksdb: This is unnecessary
						      // path.insert( path.end(), 1);
                        // You should use
                        path.push_back(1);
                        // But why are you storing a 
                        // huge where each entry is 
                        // the number 1? It would be
                        // more appropriate just to keep
                        // an integer counter
					      }
					      mf = m+1;
					      if ((mf = valid_m(mf,lf))) {
						      path.insert( path.end(), 1);
					      }
					      mf = m-1;
					      if ((mf = valid_m(mf,lf))) {
						      path.insert( path.end(),1);
					      }
				      }
			      }
			      lf = l+1;
			      if ((lf = valid_l(lf,nf))) {
				      for (int m = 0; m <= 2*l; ++m) {
					      int mf = m;
					      if ((mf = valid_m(mf,lf))) {
						      path.insert( path.end(), 1);
					      }
					      mf = m+1;
					      if ((mf = valid_m(mf,lf))) {
						      path.insert( path.end(), 1);
					      }
					      mf = m-1;
					      if ((mf = valid_m(mf,lf))) {
						      path.insert( path.end(), 1);
					      }
				      }
			      }
		      }
		      int pt = path.size();
 		 cout << n << "->" << nf << "	" << pt << "	" << Edelt << endl;
      }
   }
   }
   return 0;
}

