#include <cassert>

#include <iostream>
using std::cout;
using std::endl;

#include <iomanip>
using std::setw;

#include <cmath>
using std::sqrt;

#include <algorithm>
using std::count;

#include <vector>
using std::vector;

int main()
{int N = 1;	
 while (N <= 9) {
 	N++;
 	vector<int> tripval;
   	for (int k = 2; k <= N; ++k) {
   		for (int j = 1; j < k; ++j) {
   			for (int i = 0; i < j; ++i) {
   				int sum = i+j+k;
   				tripval.insert (tripval.end(), sum);
			}
		}	
	}
	int num = tripval.size();	
   	cout <<  N  << "	" << num << endl;
 }
   return 0;
}

