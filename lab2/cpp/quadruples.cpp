#include <cassert>

#include <iostream>
using std::cout;
using std::endl;

#include <iomanip>
using std::setw;

#include <cmath>
using std::sqrt;

#include <algorithm>
using std::count;
using std::sort;
using std::unique;

#include <vector>
using std::vector;

int main() {
   int N = 0;   
   while (N <= 99) {
      vector<int> tripval;
      vector<int> sqrval;
      for (int l = 0; l <= N; ++l) {
         for (int k = 0; k <= l; ++k) {
            for (int j = 0; j <= k; ++j) {
               for (int i =0; i <= j; ++i) {
                  // ksdb: make this const
                  const int sumi = (i*i)+(j*j)+(k*k)+(l*l);
                  // ksdb: This is not idiomatic 
                  // tripval.insert (tripval.end(), sumi);
                  // Instead
                  tripval.push_back(sumi);
                  // ksdb: It is better to check 
                  // whether n*n == sumi rather than
                  // check whether n == sqrt(sumi);
                  // ksdb: Let's just remove this 
                  // float root = sqrt(sumi);
                  // ksdb: replace float with int
                  // for (float n = 0; n <= 4*N; ++n) {
                  for (int n = 0; n <= 4*N; ++n) {
                     // ksdb: You're assigning n to root
                     // if ( (root = n) ) {
                     // You intended (root == n)
                     // if (root == n) {
                     if (n*n == sumi) {
                        // sqrval.insert( sqrval.end(), sumi);
                        sqrval.push_back(sumi);
                  }
               }
            }
         }
      }   
   }
   int num = tripval.size();   
   sort (sqrval.begin(), sqrval.end());
   auto new_end_it = unique(sqrval.begin(), sqrval.end());
   int sqr = ptrdiff_t ( new_end_it - sqrval.begin( ));
      cout <<  N  << "   " << num << "   " << sqr << endl;
   N++;
 }
   return 0;
}

