## Phys 540 Summer 2021

Code for the first lab is stored in a directory
named `lab1`.

All codes are written in C++. 

### Lab 1

Programs `hello.cpp`, `binomial.cpp`, and `maxint.cpp` all 
compile and run. The binomial program works correctly, except
that the internal working variable is a `float`. It would be
preferable to compute an exact integer. Or if not, there's no
reason not to use a `double`. 

    $ ./binomial 24 | head -n 12
    (24 choose 0) = 1
    (24 choose 1) = 24
    (24 choose 2) = 276
    (24 choose 3) = 2024
    (24 choose 4) = 10626
    (24 choose 5) = 42504
    (24 choose 6) = 134596
    (24 choose 7) = 346104
    (24 choose 8) = 735471
    (24 choose 9) = 1.3075e+06
    (24 choose 10) = 1.96126e+06
    (24 choose 11) = 2.49614e+06

    $ ./maxint | tail -n 5
    2^28 = 268435456
    2^29 = 536870912
    2^31 = -2147483648
    Error, Check last Result
    2^32 = 0

### Lab 2

The makefile as given can't compile the C++ codes, because they
have been moved to subdirectories. Also, no `quadruples` rule was
provided. The directory structure is now flattened and the
makefile updated.

In `quadruples.cpp`, it is better to check `n*n == sumi` with int
rather than `n == sqrt(sumi)` with float. The counting is off.
Much simpler to use a counter than to fill vectors.

In `selection.cpp` the main error is that `nf=1` was used in
place of the boolean test `nf==1`. This causes an infinite 
loop. The wavelength calculation and visible light annotation
are not implemented.

In `moments.cpp` the `verify_singlet()` funciton body has been
populated. Otherwise, the program is not generating the required
table of values.

### Lab 3

Again, the directory structure is not reflected in the makefile
and so programs do not compile.

The `curves.cpp` program produces the correct output. The 
`bitwise.cpp` program works but the final assert fails where `1/0` 
is used instead of `1.0/0.0`. `fp.cpp`, and `min.cpp`, all work properly.

The `denorm.cpp` program incorrectly tries to use `2^i` as
exponentiation. One should use `1 << i` instead. Or, better yet, simply
multiply by one half at every stage of the loop.

No `sum.cpp` file was originially included. The program file present now
has some problems. First, integer division (`1/3 == 0` whereas `1.0/3 ==
0.33333`) prevents the summation variable from accumulating any weight.
Second, all the calculation are done with doubles (64-bit) rather than
floats (32-bit), so the tally-up/tally-down discrepancy you were meant
to produce does not actually occur.

None of the functons bodies in `add_methods.cpp` has been filled in.

    $ seq 100000 | awk '{ print $1*$1 }' | ./sumdata
    I've read in 100000 elements
                                      s/eeeeeeee/fffffffffffffffffffffff
    conventional sum: 3.3333845e+14 : 0 10101111 00101111001010110110100
      increasing sum: 0.0000000e+00 : 0 00000000 00000000000000000000000
      decreasing sum: 0.0000000e+00 : 0 00000000 00000000000000000000000
        pairwise sum: 0.0000000e+00 : 0 00000000 00000000000000000000000
     compensated sum: 0.0000000e+00 : 0 00000000 00000000000000000000000
      high-prec. sum: 0.0000000e+00 : 0 00000000 00000000000000000000000

Modified code has been uploaded to the repository. The `sum.cpp` program
now works correctly.

    $ ./sum
                      10             2.92897             2.92897
                     100             5.18738             5.18738
                    1000             7.48548             7.48547
                   10000             9.78761              9.7876
                  100000             12.0909             12.0902
                 1000000             14.3574             14.3927
                10000000             15.4037              16.686

And the `add_methods.cpp` file has been filled out so that `sumdata.cpp`
now produces nontrivial output.

    $ seq 100000 | awk '{ print $1*$1 }' | ./sumdata
    I've read in 100000 elements
                                      s/eeeeeeee/fffffffffffffffffffffff
    conventional sum: 3.3333845e+14 : 0 10101111 00101111001010110110100
      increasing sum: 3.3333845e+14 : 0 10101111 00101111001010110110100
      decreasing sum: 3.3332671e+14 : 0 10101111 00101111001010001010110
        pairwise sum: 3.3333822e+14 : 0 10101111 00101111001010110101101
     compensated sum: 3.3333832e+14 : 0 10101111 00101111001010110110000
      high-prec. sum: 3.3333832e+14 : 0 10101111 00101111001010110110000

I like the elegant solution to `add_high_precision()`, in which the work
is all done by adding a long-double type literal to the initializer zero.

    return accumulate(v.begin(),v.end(),0.0L);

The `add_pairwise()` is not quite correct. First, it does not correctly
handle the case where the input vector has an odd number of elements.
Second, I was intending it to be computed *recursively* pairwise

    A = [A0, A1, A2, A3, A4, A5, A6, A7]
          \                          /
           A0+A1, A2+A3, A4+A5, A6+A7
           \                       /
            A0+A1+A2+A3,A4+A5+A6+A7
             \                   /

