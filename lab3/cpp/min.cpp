#include <iostream>
using std::cout;
using std::endl;

#include <limits>
using std::numeric_limits;

#include <iomanip>
using std::setw;

#include <cmath>
using std::log;

int main()
{
   const int pf = std::numeric_limits<float>::digits10;
   const int pd = std::numeric_limits<double>::digits10;
   const int pld = std::numeric_limits<long double>::digits10;
   const int pad = 10;

   float f = 1.0F;
   double d = 1.0;
   long double ld = 1.0L;

   const float fzero = 0.0F; 
   const double dzero = 0.0; 
   const long double lzero = 0.0L; 

   cout.setf(std::ios::scientific);
   while (true)
   {  
     if (f == fzero) {
	     cout.precision(pf-1);
	     cout << setw(pf+pad) << "exact zero";
     } else {
      cout.precision(pf-1);
      cout << setw(pf+pad) << f;
     }
     if (d == dzero) {
	     cout.precision(pd-1);
	     cout << setw(pd+pad) << "exact zero";
     } else {
      cout.precision(pd-1);
      cout << setw(pd+pad) << d;
     }
     if (ld == lzero) {
	     cout.precision(pld-1);
	     cout << setw(pld+pad) << "exact zero";
     } else {
      cout.precision(pld-1);
      cout << setw(pld+pad) << ld << endl; 
     }
      

      if (f == fzero and d == dzero and ld == lzero) break;

      f /= 10.0F;
      d /= 10.0;
      ld /= 10.0L;
   }

   return 0;
}

