#include <iostream>
using std::cout;
using std::endl;

template <typename T>
void report(T a, char op, T b, T c)
{
   cout << a << " " << op << " " << b << " = " << c << endl;
}

int main()
{
   const float one = 1.0F;
   const float neg_one = -1.0F;
   float zero = one; --zero; // this tricks the compiler

   cout << "Floating point division:" << endl;

   report(one,'/',zero,one/zero);
   report(neg_one,'/',zero,neg_one/zero);
   report(zero,'/',zero,zero/zero*neg_one);
   report(one/zero,'/',zero,one/zero);
   report(neg_one/zero,'/',zero,neg_one/zero);
   report(one/zero,'/',one/zero,zero/zero*neg_one);

   cout << endl << "Floating point multiplication:" << endl;

   report(one,'*',zero,zero);
   report(neg_one,'*',zero,neg_one*zero);
   report(one,'*',one/zero,one/zero);
   report(neg_one,'*',one/zero,neg_one/zero);
   report(zero,'*',one/zero,zero/zero*neg_one);
   report(neg_one/zero,'*',zero,zero/zero*neg_one);
   report(one/zero,'*',one/zero,one/zero);
   report(one/zero,'*',neg_one/zero,neg_one/zero);
   report(one,'*',zero/zero*neg_one,zero/zero*neg_one);
   report(neg_one,'*',zero/zero*neg_one,zero/zero*neg_one);
   report(zero,'*',zero/zero*neg_one,zero/zero*neg_one);
   report(zero*neg_one,'*',zero/zero*neg_one,zero/zero*neg_one);

   cout << endl << "Floating point addition and subtraction" << endl;

   report(one,'+',one/zero,one/zero);
   report(one,'-',one/zero,neg_one/zero);
   report(one/zero,'+',one/zero,one/zero);
   report(one/zero,'-',one/zero,zero/zero*neg_one);
   report(one,'+',zero/zero*neg_one,zero/zero*neg_one);
   report(one,'-',zero/zero*neg_one,zero/zero*neg_one);



   return 0;
}
