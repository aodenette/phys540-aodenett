#include <iostream>
using std::cout;
using std::endl;

#include <iomanip> // header file for special iostream manipulators
using std::setw;   // sets the character width of the output

#include <cmath>
using std::pow;

int main()
{
   for (int i = 1; i <= 9; ++i)       
   {          
	   unsigned long int N = pow(10,i);
      float asc = 0.0;
      float des = 0.0;
      for (float na = 1.0; na <= N; ++na)
      {
	      float asum = (1.0/na);
	      asc += asum;
      }
      for (float nd = N; nd >= 1.0; --nd)
      {
	      float dsum = (1.0/nd);
	      des += dsum;
      }


      cout << setw(20) << N << setw(20)  << asc << setw(20) << des << endl;
   }
   return 0;
}

