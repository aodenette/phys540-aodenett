#include <cassert>

#include <vector>
using std::vector;

#include <algorithm>
using std::sort;

#include <numeric>
using std::accumulate;

float add_pairwise(const vector<float> &v)
{
   float sump = v[0];
   int lim = v.size()/2;
   for ( int i = 1; i <= lim; ++i) {
   sump += (v[i] + v[lim+i]);
   }
   return sump; 
}

float add_increasing(const vector<float> &v)
{
    float sumi = v[0];
    for (int i = 1; i < v.size(); ++i)
       sumi += v[i];
    return sumi;
}

float add_decreasing(const vector<float> &v)
{
   float sumd = v[0];
   for (int i = v.size(); i > 0; --i)
      sumd += v[i];
   return sumd;
}

float add_conventional(const vector<float> &v)
{
   return accumulate(v.begin(),v.end(),0.0F);
   // last line is equivalent to the following:
   // float sum = v[0];
   // for (int i = 1; i < v.size(); ++i)
   //    sum += v[i];
   // return sum;
}

float add_compensated(const vector<float> &v)
{
   float sumc = v[0];
   float c = 0.0;
   for (int i = 1; i <= v.size(); ++i) {
      float y = v[i] - c;
      float t = sumc + y;
      c = (t -sumc) - y;
      sumc = t;
   }
   return sumc;
}

float add_high_precision(const vector<float> &v)
{
   return accumulate(v.begin(),v.end(),0.0L);
}

