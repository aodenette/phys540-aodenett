set terminal dumb ansi256

unset key
set xlabel "x"
set ylabel "V(x)"
plot[0.7:1.5][-1:1] "energywell.dat" using 1:2 w l

pause -1

set size square
set xlabel "x"
set ylabel "y"
plot "positions.dat" using 1:2 w p

while (1) {
   replot
   pause 1
}
