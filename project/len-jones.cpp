#include <cstdlib>
using std::size_t;
using std::exit;
using std::atoi;

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include <iomanip> 
using std::setw;  

// ksdb: The correct include is cmath, not math.h
#include <cmath>
using std::pow;
using std::cos;
using std::sin;

#include <vector>
using std::vector;

#include <fstream>
using std::ofstream;

#include <random>
using std::mt19937;
using std::uniform_real_distribution;

// mersenne twistor random number generator
static mt19937 rng; 
// random fp number in [0,1)
static auto rnd = bind(uniform_real_distribution<double>(0,1),std::ref(rng));
// random fp number is [-1,1)
static auto rnd1 = bind(uniform_real_distribution<double>(-1,1),std::ref(rng));

class coord
{
   public:
      double x,y;
      coord(double x_, double y_) : x(x_), y(y_) {}
};

double dist(const coord &A, const coord &B)
{
   const double dx = B.x - A.x;
   const double dy = B.y - A.y;
   return sqrt(dx*dx+dy*dy);
}

vector<coord> particle;

double lj(double x) // Lennard-Jones pair potential
{
   // ksdb: The pow function is very expensive and should
   // generally be reserved for when you have to take the
   // fractional power of some number
   //
   // return ((1.0/(pow (x, 12.0)))-(2.0/(pow (x, 6.0))));

   // Try something like this
   const double x3 = x*x*x;
   const double inv_x6 = 1.0/x3/x3;
   const double inv_x12 = inv_x6*inv_x6;
   return inv_x12 - 2.0*inv_x6;
}

double pe(void) // potential energy summed pairwise
{
   double sum = 0.0;
   // sum over all pairs with no double counting
   for (size_t j = 1; j < particle.size(); ++j)
      for (size_t i = 0; i < j; ++i)
         sum += lj(dist(particle[i],particle[j]));
   return sum;
}

void initialize(int N)
{
   for (int n = 0; n < N; ++n)
   {
      const double r = 0.25*n;
      const double t = 6.0*n*M_PI/(N-1);
      const double x = r*cos(t);
      const double y = r*sin(t);
      particle.push_back(coord(x,y));
   }
}

void write_positions(void)
{
   ofstream fout("positions.dat");
   for (auto p : particle)
      fout << setw(20) << p.x << setw(20) << p.y << endl;
   fout << endl;
   fout << endl;
   fout.close();
   fout.clear();
}

void usage(void)
{
   cerr << "Usage: len-jones [2 < #particles < 100]" << endl;
   exit(1);
}

int main(int argc, char* argv[])
{
   if (argc != 2) usage();
   {
      int N = atoi(argv[1]);
      if (N < 2 or N >= 100) usage();
      initialize(N);
   }
   cout.precision(6);
   ofstream fout("energywell.dat");

   // ksdb: i is an integer. Why are you initializing to 0.00001?
   // for (int i = 0.00001; i <= 3000; ++i)
   for (int i = 300; i <= 2000; ++i)
   {
      const double x = i*0.001;
      fout << setw(20) << x << setw(20) << lj(x)  << endl;   
   }
   fout.close();
   fout.clear();

   write_positions();

   double Emin = pe();
   double Ecurrent = Emin;
   size_t moves = 0;
   while (true)
   {
      // propose moves to the particles
      vector<coord> tmp = particle;
      
      for (auto &p : particle)
      {
         if (rnd() < 3.0/particle.size())
         {
            const double step = sqrt(rnd());
            p.x += step*rnd1();
            p.y += step*rnd1();
         }
      }
      const double Enew = pe();
      if (Enew < Ecurrent)
      {
         // accept the moves
         Ecurrent = Enew;
         ++moves;
         if (Enew < Emin)
         {
            Emin = Enew;
            cout << moves << ": " << Emin << " --> " << Enew << endl;
         }
         //if (moves%20 == 0) 
            write_positions();
      }
      else
      {
         // revert to the old configuration
         swap(particle,tmp);
      }
        }
   return 0;
}

